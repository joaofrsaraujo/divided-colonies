##################################################################################################
# scriptable and usable for event-system below.
##################################################################################################

orinoco_estuary_modifier = {
	province_trade_power_value = 2
	picture = "estuary_icon"
}

amazon_estuary_modifier = {
	province_trade_power_value = 2
	picture = "estuary_icon"
}

caribbean_trade_modifier = {
	province_trade_power_value = 6
	picture = "province_trade_power_value"
}

strategic_early_african_port = {
	province_trade_power_value = -10
	picture = "province_trade_power_value"
}